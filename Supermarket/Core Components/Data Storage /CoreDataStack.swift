//
//  CoreDataStack.swift
//  supermarket
//
//  Created by Valery Rublevskaya on 15/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStack {
    
    private var modelName = "supermarket"
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: self.modelName)
        container.loadPersistentStores { persistentStoreDescription , error in
            guard error == nil else { fatalError(".loadPersistentStores failed") }
        }
        container.viewContext.automaticallyMergesChangesFromParent = true
        return container
    }()
    
    lazy var mainManagedObjectContext: NSManagedObjectContext = {
        return self.persistentContainer.viewContext
    }()
    
    init(with modelName: String) {
        self.modelName = modelName
    }
    
    func save()  {
        guard mainManagedObjectContext.hasChanges else { return }
        do {
            try mainManagedObjectContext.save()
        } catch let error as NSError {
            print(".save() context. Unresolved error  \(error), \(error.userInfo)")
        }
    }
    
    func populateDataIfNeeded() {
        let fetchedRequest: NSFetchRequest<Good> = Good.fetchRequest()
        do {
            let count = try mainManagedObjectContext.count(for: fetchedRequest)
            if count == 0 {
                populateData()
            }
        } catch let error as NSError {
            print("Cannot count Goods. Unresolved error  \(error), \(error.userInfo)")
        }
    }
    
    private func populateData() {
        guard let array = DataSerialize.serializeFile(resource: "goods", serializator: JSONSerializator()) else { return }
        let childContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        childContext.parent = mainManagedObjectContext
        var goods: [Good] = []
        for dict in array {
            guard let good = Good.good(from: dict, withContext: childContext) else { return }
            goods.append(good)
        }
        do {
            try childContext.save()
            save()
        } catch let error as NSError {
            print("Cannot save childContext. Unresolved error  \(error), \(error.userInfo)")
        }
    }
}
