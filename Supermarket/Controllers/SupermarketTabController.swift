//
//  SupermarketTabController.swift
//  Supermarket
//
//  Created by Valery Rublevskaya on 15/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import UIKit

class SupermarketTabController: UITabBarController {

    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureController()
    }
    
    //MARK: - Configuration
    private func configureController() {
        if let items = tabBar.items {
            for item in items {
                item.selectedImage = item.selectedImage?.withRenderingMode(.alwaysOriginal)
            }
        }
        
    }

}

