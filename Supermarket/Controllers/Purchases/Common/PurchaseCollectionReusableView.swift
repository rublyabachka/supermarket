//
//  PurchaseCollectionReusableView.swift
//  Supermarket
//
//  Created by Valery Rublevskaya on 15/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import UIKit

class PurchaseCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet private weak var goodCategoryLabel: UILabel!
    
    func configure(with text: String) {
        goodCategoryLabel.text = text
    }
}
