//
//  PurchaseCollectionViewCell.swift
//  Supermarket
//
//  Created by Valery Rublevskaya on 15/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import UIKit

class PurchaseCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private weak var goodImageView: UIImageView!
    @IBOutlet private weak var goodNameLabel: UILabel!
    @IBOutlet private weak var goodPriceLabel: UILabel!
    @IBOutlet private weak var goodCountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(with good: Good) {
        goodNameLabel.text = "\(good.name)"
        goodPriceLabel.text = "\(good.price) $"
        goodCountLabel.text = "\(good.count) шт."
    }
}
