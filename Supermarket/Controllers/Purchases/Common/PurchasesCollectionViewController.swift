//
//  PurchasesCollectionViewController.swift
//  Supermarket
//
//  Created by Valery Rublevskaya on 15/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import UIKit
import CoreData

class PurchasesCollectionViewController: UICollectionViewController {
    
    var coreDataStack: CoreDataStack!
    
    var fetchedResultsController: NSFetchedResultsController<Good>!
    var cacheName = "supermarket.purchase"
    
    var blockOperations: [BlockOperation] = []
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureController()
    }
    
    // MARK: - Methods
    
    private func configureController() {
        collectionView?.contentInset = UIEdgeInsets(top: 20, left: 10, bottom: 20, right: 10)
        collectionView?.registerCellType(PurchaseCollectionViewCell.self)
        
        let fetchRequest: NSFetchRequest<Good> = Good.fetchRequest()
        let categorySortDescriptor = NSSortDescriptor(key: #keyPath(Good.category.name), ascending: true)
        let priceSortDescriptor = NSSortDescriptor(key: #keyPath(Good.price), ascending: true)
        fetchRequest.sortDescriptors = [categorySortDescriptor, priceSortDescriptor]
        let mainContext = coreDataStack.mainManagedObjectContext
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: mainContext, sectionNameKeyPath: #keyPath(Good.category.name), cacheName: cacheName)
        fetchedResultsController.delegate = self
        do {
            try fetchedResultsController.performFetch()
        } catch let error {
            print(error)
        }
        
    }
    
    fileprivate func performFetch() {
        do {
            try fetchedResultsController.performFetch()
            collectionView?.reloadData()
        } catch let error {
            print(error)
        }
    }
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return fetchedResultsController.sections!.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let sectionInfo = fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PurchaseCollectionViewCell.stringClass, for: indexPath) as! PurchaseCollectionViewCell
        let good = fetchedResultsController.object(at: indexPath)
        cell.configure(with: good)
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: PurchaseCollectionReusableView.stringClass, for: indexPath) as! PurchaseCollectionReusableView
            let good = fetchedResultsController.object(at: indexPath)
            headerView.configure(with: good.category.name)
            return headerView
        default:
            assert(false, "Unexpected element kind")
        }
    }
    
    // MARK: UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let pageViewController = self.storyboard?.instantiateViewController(withIdentifier: PurchasePageViewController.stringClass) as? PurchasePageViewController {
            let sectionInfo = fetchedResultsController.sections![indexPath.section]
            pageViewController.count = sectionInfo.numberOfObjects
            pageViewController.currentIndex = indexPath.row
            pageViewController.goods = fetchedResultsController.sections![indexPath.section].objects as! [Good]
            pageViewController.coreDataStack = coreDataStack
            present(pageViewController, animated: true)
        }
    }
}

// MARK: UICollectionViewDelegateFlowLayout

extension PurchasesCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 150)
    }
}

//MARK: NSFetchedResultsControllerDelegate
extension PurchasesCollectionViewController: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    }
    
    func controller( _ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            guard let newIndexPath = newIndexPath else { return }
            blockOperations.append(BlockOperation { [weak self] in
                self?.collectionView?.insertItems(at: [newIndexPath])
            })
        case .move:
            guard let indexPath = indexPath,
                let newIndexPath = newIndexPath else { return }
            if indexPath == newIndexPath { fallthrough }
            blockOperations.append(BlockOperation { [weak self] in
                self?.collectionView?.moveItem(at: indexPath, to: newIndexPath)
            })
        case .update:
            guard let indexPath = indexPath else { return }
            blockOperations.append(BlockOperation { [weak self] in
                guard let strongSelf = self else { return }
                let cell = strongSelf.collectionView?.cellForItem(at: indexPath) as? PurchaseCollectionViewCell
                let good = strongSelf.fetchedResultsController.object(at: indexPath)
                cell?.configure(with: good)
            })
        case .delete:
            guard let indexPath = indexPath else { return }
            blockOperations.append(BlockOperation { [weak self] in
                self?.collectionView?.deleteItems(at: [indexPath])
            })
        }
    }
    
    func controller( _ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            blockOperations.append(BlockOperation { [weak self] in
                self?.collectionView?.insertSections(IndexSet(integer: sectionIndex))
            })
        case .delete:
            blockOperations.append(BlockOperation { [weak self] in
                self?.collectionView?.deleteSections(IndexSet(integer: sectionIndex))
            })
        case .update, .move:
            break
        }
    }
    
    func controllerDidChangeContent( _ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        collectionView?.performBatchUpdates({ 
            self.blockOperations.forEach { $0.start() }
        }, completion: { (_) in
            self.blockOperations.removeAll(keepingCapacity: false)
        })
    }
    
}
