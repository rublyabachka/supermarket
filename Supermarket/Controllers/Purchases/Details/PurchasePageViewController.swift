//
//  PurchasePageViewController.swift
//  Supermarket
//
//  Created by Valery Rublevskaya on 17/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import UIKit

class PurchasePageViewController: UIPageViewController {
    
    var coreDataStack: CoreDataStack!
    
    var goods: [Good] = []
    var currentIndex = 0
    var count = 0
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareViewController()
    }
    
    private func prepareViewController() {
        prepareDataSource()
        prepareAppearance()
        setupPageRootViewController()
    }
    
    private func prepareDataSource() {
        dataSource = self
    }
    
    private func prepareAppearance() {
        view.backgroundColor = .white
        let pageControl = UIPageControl.appearance()
        pageControl.pageIndicatorTintColor = .darkGray
        pageControl.currentPageIndicatorTintColor = UIColor.gray
        pageControl.backgroundColor = .white
        pageControl.tintColor = .white
    }
    
    private func setupPageRootViewController() {
        let firstViewController = purchaseDetailViewController(at: currentIndex)
        setViewControllers([firstViewController], direction: .forward, animated: false, completion: nil)
    }
    
    fileprivate func purchaseDetailViewController(at index: Int) -> PurchaseDetailViewController {
        let controller = storyboard!.instantiateViewController(withIdentifier: PurchaseDetailViewController.stringClass) as! PurchaseDetailViewController
        controller.pageIndex = index
        controller.good = goods[index]
        controller.coreDataStack = coreDataStack
        return controller
    }
}

// MARK: - PageViewController Data Source

extension PurchasePageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let controller = viewController as? PurchaseDetailViewController else { return nil }
        let index = controller.pageIndex!
        guard index >= 0 && index + 1 < goods.count else { return nil }
        return purchaseDetailViewController(at: index + 1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let controller = viewController as? PurchaseDetailViewController else { return nil }
        let index = controller.pageIndex!
        guard index >= 1 && index < goods.count else { return nil }
        return purchaseDetailViewController(at: index - 1)
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return currentIndex
    }
    
}
