//
//  PurchaseDetailViewController.swift
//  Supermarket
//
//  Created by Valery Rublevskaya on 17/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import UIKit

class PurchaseDetailViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet private weak var titleNavigationItem: UINavigationItem!
    @IBOutlet private weak var goodsPriceLabel: UILabel!
    @IBOutlet private weak var goodsCountLabel: UILabel!
    @IBOutlet private weak var countBuyTextField: UITextField!
    @IBOutlet private weak var summaryLabel: UILabel!
    @IBOutlet private weak var countStepper: UIStepper!
    @IBOutlet private weak var buyButton: UIButton!
    @IBOutlet private weak var scrollView: UIScrollView!
    
    var coreDataStack: CoreDataStack!
    
    var isUserEditing = false
    
    var pageIndex: Int!
    var good: Good!
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureController()
    }
    
    // MARK: - Actions
    
    @IBAction func closeButtonClick(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func textFieldEditing(_ sender: UITextField) {
        updateBuyButton()
    }
    
    @IBAction func countStepperValueChanged(_ sender: UIStepper) {
        let count = Int(sender.value)
        countBuyTextField.text = count.description
        calculateTotalValue(with: count)
        updateBuyButton()
    }
    
    @IBAction func buyButtonClick(_ sender: UIButton) {
        guard let count = Int32(countBuyTextField.text!) else { return }
        countBuyTextField.text = ""
        countStepper.value = 0
        let queue = DispatchQueue.global(qos: .userInitiated)
        queue.async {
            sleep(3)
            DispatchQueue.main.async {
                self.good.count -= count
                self.coreDataStack.save()
                if self.shouldDeleteGood() {
                    self.deleteGood()
                }
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    private func shouldDeleteGood() -> Bool {
        return good.count == 0 ? true : false
    }
    
    private func deleteGood() {
        coreDataStack.mainManagedObjectContext.delete(good)
        coreDataStack.save()
    }
    
    private func updateController() {
        titleNavigationItem.title = good.name
        goodsPriceLabel.text = String(good.price)
        goodsCountLabel.text = String(good.count)
    }
    
    private func updateBuyButton() {
        if let text = countBuyTextField.text, let count = Int(text), count > 0 {
            enableBuyButton()
        } else {
            disableBuyButton()
        }
    }
    
    // MARK: - Methods
    
    private func configureController() {
        countBuyTextField.delegate = self
        countStepper.minimumValue = 0
        countStepper.maximumValue = Double(good.count)
        
        updateController()
        updateBuyButton()
        addKeyboardNotifications()
        addGestureRecognizer()
    }
    
    private func enableBuyButton() {
        buyButton.isEnabled = true
        buyButton.alpha = 1
    }
    
    private func disableBuyButton() {
        buyButton.isEnabled = false
        buyButton.alpha = 0.5
    }
    
    private func calculateTotalValue(with count: Int) {
        let goodsPrice = good.price
        let price = Double(count) * goodsPrice
        summaryLabel.text = String(price)
    }
    
    // MARK: - Keyboard show logic
    
    private func addKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: Notification.Name.UIKeyboardWillShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: Notification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    private func addGestureRecognizer() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc private func hideKeyboard(gesture: UIGestureRecognizer?) {
        view.endEditing(true)
    }
    
    private func shouldIncreaseView() -> Bool {
        return isUserEditing
    }
    
    private func increaseView(withDuration duration: TimeInterval, curve: UInt) {
        UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions(rawValue: curve), animations: {
            self.scrollView.contentInset = .zero
        })
        isUserEditing = false
    }
    
    private func shouldDecreaseTopContainerView(keyboardRect: CGRect) -> Bool {
        return isUserEditing == false
    }
    
    private func decreaseView(withDuration duration: TimeInterval, curve: UInt, keyboardRect: CGRect) {
        isUserEditing = true
        UIView.animate(withDuration: duration, delay: 0.0, options: .init(rawValue: curve), animations: {
            self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardRect.height, right: 0)
        }, completion: nil)
    }
    
    // MARK: - Notification Logics
    
    @objc private func keyboardWillShow(aNotification: Notification) {
        if let userInfo = aNotification.userInfo,
            let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval,
            let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? UInt,
            let keyboardFrameValue = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue,
            shouldDecreaseTopContainerView(keyboardRect: keyboardFrameValue.cgRectValue) {
            decreaseView(withDuration: duration, curve: curve, keyboardRect: keyboardFrameValue.cgRectValue)
        }
    }
    
    @objc private func keyboardWillHide(aNotification: Notification) {
        if let userInfo = aNotification.userInfo,
            let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? Double,
            let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? UInt,
            shouldIncreaseView() {
            increaseView(withDuration: duration, curve: curve)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        guard let stringRange = range.range(for: currentText) else { return false }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        guard updatedText.isEmpty == false else {
            calculateTotalValue(with: 0)
            return true
        }
        guard let updatedCount = Int32(updatedText) else { return false }
        guard updatedCount <= good.count else { return false }
        countStepper.value = Double(updatedCount)
        calculateTotalValue(with: Int(updatedCount))
        return true
    }
}
