//
//  SalesTableViewController.swift
//  Supermarket
//
//  Created by Valery Rublevskaya on 18/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import UIKit
import CoreData

class SalesTableViewController: UITableViewController {
    
    var coreDataStack: CoreDataStack!
    
    var fetchedResultsController: NSFetchedResultsController<Good>!
    var cacheName = "supermarket.sales"

    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureController()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections!.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SalesTableViewCell.stringClass, for: indexPath) as! SalesTableViewCell
        let good = fetchedResultsController.object(at: indexPath)
        cell.configure(with: good)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return fetchedResultsController.sections?[section].name
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            coreDataStack.mainManagedObjectContext.delete(fetchedResultsController.object(at: indexPath))
            coreDataStack.save()
        }
    }
    
    // MARK: - Actions
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let pageViewController = self.storyboard?.instantiateViewController(withIdentifier: SalesPageViewController.stringClass) as? SalesPageViewController {
            let sectionInfo = fetchedResultsController.sections![indexPath.section]
            pageViewController.count = sectionInfo.numberOfObjects
            pageViewController.currentIndex = indexPath.row
            pageViewController.goods = fetchedResultsController.sections![indexPath.section].objects as! [Good]
            pageViewController.coreDataStack = coreDataStack
            present(pageViewController, animated: true)
        }
    }

    @IBAction func addGoodForSale(_ sender: UIBarButtonItem) {
        if let newGoodViewController = self.storyboard?.instantiateViewController(withIdentifier: SalesDetailViewController.stringClass) as? SalesDetailViewController {
            newGoodViewController.coreDataStack = coreDataStack
            present(newGoodViewController, animated: true)
        }
    }
    
    // MARK: - Methods
    
    private func configureController() {
        let fetcherRequest: NSFetchRequest<Good> = Good.fetchRequest()
        let categorySortDescriptor = NSSortDescriptor(key: #keyPath(Good.category.name), ascending: true)
        let priceSortDescriptor = NSSortDescriptor(key: #keyPath(Good.price), ascending: true)
        fetcherRequest.sortDescriptors = [categorySortDescriptor, priceSortDescriptor]
        let mainContext = coreDataStack.mainManagedObjectContext
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetcherRequest, managedObjectContext: mainContext, sectionNameKeyPath: #keyPath(Good.category.name), cacheName: cacheName)
        fetchedResultsController.delegate = self
        do {
            try fetchedResultsController.performFetch()
        } catch let error {
            print(error)
        }
    }
    
    private func performFetch() {
        do {
            try fetchedResultsController.performFetch()
            tableView.reloadData()
        } catch let error {
            print(error)
        }
    }

}

//MARK: NSFetchedResultsControllerDelegate
extension SalesTableViewController: NSFetchedResultsControllerDelegate {
        
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            guard let newIndexPath = newIndexPath else { return }
            tableView.insertRows(at: [newIndexPath], with: .automatic)
        case .move:
            guard let indexPath = indexPath,
                let newIndexPath = newIndexPath else { return }
            if indexPath == newIndexPath { fallthrough }
            tableView.moveRow(at: indexPath, to: newIndexPath)
        case .update:
            guard let indexPath = indexPath else { return }
            let cell = tableView.cellForRow(at: indexPath) as? SalesTableViewCell
            let good = fetchedResultsController.object(at: indexPath)
            cell?.configure(with: good)
        case .delete:
            guard let indexPath = indexPath else { return }
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .automatic)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .automatic)
        case .update, .move:
            break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}

