//
//  SalesTableViewCell.swift
//  Supermarket
//
//  Created by Valery Rublevskaya on 18/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import UIKit

class SalesTableViewCell: UITableViewCell {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var countLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(with good: Good) {
        nameLabel.text = good.name
        priceLabel.text = good.price.description
        countLabel.text = good.count.description
    }

}
