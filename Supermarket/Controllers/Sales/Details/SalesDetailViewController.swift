//
//  SalesDetailViewController.swift
//  Supermarket
//
//  Created by Valery Rublevskaya on 17/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import UIKit
import CoreData

class SalesDetailViewController: UIViewController {
    
    @IBOutlet private weak var titleTextField: UITextField!
    @IBOutlet private weak var priceTextField: UITextField!
    @IBOutlet private weak var countTextField: UITextField!
    @IBOutlet private weak var categoryTextField: UITextField!
    @IBOutlet private weak var saveButton: UIButton!
    @IBOutlet private weak var scrollView: UIScrollView!
    
    var coreDataStack: CoreDataStack!
    
    var isUserEditing = false
    
    var pageIndex: Int!
    var good: Good?
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureController()
    }
    
    // MARK: - Actions
    
    private func updateSaveButton() {
        if let title = titleTextField.text,
            let category = categoryTextField.text,
            let price = Double(priceTextField.text ?? ""),
            let count = Int32(countTextField.text ?? ""),
            !title.isEmpty, !category.isEmpty, price > 0, count > 0 {
            enableSaveButton()
        } else {
            disableSaveButton()
        }
    }
    
    @IBAction func closeButtonClick(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func textFieldEditing(_ sender: UITextField) {
        updateSaveButton()
    }
    
    @IBAction func saveButtonClick(_ sender: UIButton) {
        let queue = DispatchQueue.global(qos: .userInitiated)
        queue.async {
            sleep(5)
            DispatchQueue.main.async {
                self.createOrUpdateGood()
            }
        }
        closeViewController()
    }
    
    // MARK: - Methods
    
    private func configureController() {
        updateFields()
        disableSaveButton()
        addKeyboardNotifications()
        addGestureRecognizer()
    }
    
    private func closeViewController() {
        dismiss(animated: true, completion: nil)
    }
    
    private func updateFields() {
        if let good = good {
            titleTextField.text = good.name
            priceTextField.text = good.price.description
            countTextField.text = good.count.description
            categoryTextField.text = good.category.name
        }
    }
    
    private func createOrUpdateGood() {
        if good == nil {
            good = Good(context: coreDataStack.mainManagedObjectContext)
            good?.id = Good.incrementId(type: Good.self, context: coreDataStack.mainManagedObjectContext)
        }
        good?.name = titleTextField.text!
        good?.price = Double(priceTextField.text!)!
        good?.count = Int32(countTextField.text!)!
        let fetchRequest: NSFetchRequest = Category.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "%@ == %K", categoryTextField.text!, #keyPath(Category.name))
        do {
            let categories = try coreDataStack.mainManagedObjectContext.fetch(fetchRequest)
            if let category = categories.first {
                good?.category = category
                category.addToGoods(good!)
            } else {
                let category = Category(context: coreDataStack.mainManagedObjectContext)
                category.id = Category.incrementId(type: Category.self, context: coreDataStack.mainManagedObjectContext)
                category.name = categoryTextField.text!
                good?.category = category
                category.addToGoods(good!)
            }
        } catch let error as NSError {
            print("Cannot find Category. Unresolved error  \(error), \(error.userInfo)")
        }
        coreDataStack.save()
    }
    
    private func enableSaveButton() {
        saveButton.isEnabled = true
        saveButton.alpha = 1
    }
    
    private func disableSaveButton() {
        saveButton.isEnabled = false
        saveButton.alpha = 0.5
    }
    
    // MARK: - Keyboard show logic
    
    private func addKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: Notification.Name.UIKeyboardWillShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: Notification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    private func addGestureRecognizer() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc private func hideKeyboard(gesture: UIGestureRecognizer?) {
        view.endEditing(true)
    }
    
    private func shouldIncreaseView() -> Bool {
        return isUserEditing
    }
    
    private func increaseView(withDuration duration: TimeInterval, curve: UInt) {
        UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions(rawValue: curve), animations: {
            self.scrollView.contentInset = .zero
        })
        isUserEditing = false
    }
    
    private func shouldDecreaseTopContainerView(keyboardRect: CGRect) -> Bool {
        return isUserEditing == false
    }
    
    private func decreaseView(withDuration duration: TimeInterval, curve: UInt, keyboardRect: CGRect) {
        isUserEditing = true
        UIView.animate(withDuration: duration, delay: 0.0, options: .init(rawValue: curve), animations: {
            self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardRect.height, right: 0)
        }, completion: nil)
    }
    
    // MARK: - Notification Logics
    
    @objc private func keyboardWillShow(aNotification: Notification) {
        if let userInfo = aNotification.userInfo,
            let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval,
            let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? UInt,
            let keyboardFrameValue = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue,
            shouldDecreaseTopContainerView(keyboardRect: keyboardFrameValue.cgRectValue) {
            decreaseView(withDuration: duration, curve: curve, keyboardRect: keyboardFrameValue.cgRectValue)
        }
    }
    
    @objc private func keyboardWillHide(aNotification: Notification) {
        if let userInfo = aNotification.userInfo,
            let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? Double,
            let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? UInt,
            shouldIncreaseView() {
            increaseView(withDuration: duration, curve: curve)
        }
    }
}
