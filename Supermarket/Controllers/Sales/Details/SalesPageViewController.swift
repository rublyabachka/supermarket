//
//  SalesPageViewController.swift
//  Supermarket
//
//  Created by Valery Rublevskaya on 17/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import UIKit

class SalesPageViewController: UIPageViewController {
    
    var coreDataStack: CoreDataStack!
    
    var goods: [Good] = []
    var currentIndex = 0
    var count = 0
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareViewController()
    }
    
    // MARK: - Methods
    
    private func prepareViewController() {
        prepareDataSource()
        prepareAppearance()
        setupPageRootViewController()
    }
    
    private func prepareDataSource() {
        dataSource = self
    }
    
    private func prepareAppearance() {
        view.backgroundColor = .white
        let pageControl = UIPageControl.appearance()
        pageControl.pageIndicatorTintColor = .darkGray
        pageControl.currentPageIndicatorTintColor = UIColor.gray
        pageControl.backgroundColor = .white
        pageControl.tintColor = .white
    }
    
    private func setupPageRootViewController() {
        let firstViewController = salesDetailViewController(at: currentIndex)
        setViewControllers([firstViewController], direction: .forward, animated: false, completion: nil)
    }
    
    fileprivate func salesDetailViewController(at index: Int) -> SalesDetailViewController {
        let controller = storyboard!.instantiateViewController(withIdentifier: SalesDetailViewController.stringClass) as! SalesDetailViewController
        controller.pageIndex = index
        controller.good = goods[index]
        controller.coreDataStack = coreDataStack
        return controller
    }

    /*
    private func configureController() {
        dataSource = self
        view.backgroundColor = .white
        setViewControllers([getViewController(at: 0)!], direction: .forward, animated: true, completion: nil)
        configurePageControl()
    }
    
    private func configurePageControl() {
        let pageControl = UIPageControl.appearance()
        pageControl.pageIndicatorTintColor = .darkGray
        pageControl.currentPageIndicatorTintColor = UIColor.gray
        pageControl.backgroundColor = .white
        pageControl.tintColor = .white
    }
    */
}

// MARK: - PageViewController Data Source

extension SalesPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let controller = viewController as? SalesDetailViewController else { return nil }
        let index = controller.pageIndex!
        guard index >= 0 && index + 1 < goods.count else { return nil }
        return salesDetailViewController(at: index + 1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let controller = viewController as? SalesDetailViewController else { return nil }
        let index = controller.pageIndex!
        guard index >= 1 && index < goods.count else { return nil }
        return salesDetailViewController(at: index - 1)
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return currentIndex
    }
}
