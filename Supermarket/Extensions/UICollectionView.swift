//
//  UICollectionView.swift
//  Supermarket
//
//  Created by Valery Rublevskaya on 15/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import UIKit

extension UICollectionView {
    func registerCellType<T: UICollectionViewCell>(_ type: T.Type) {
        registerCellType(type, reuseIdentifier: type.stringClass)
    }
    
    func registerCellType<T: UICollectionViewCell>(_ type: T.Type, reuseIdentifier: String!) {
        let nib = UINib(nibName: type.stringClass, bundle: nil)
        register(nib, forCellWithReuseIdentifier: reuseIdentifier)
    }
}
