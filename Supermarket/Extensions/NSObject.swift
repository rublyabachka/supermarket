//
//  NSObject.swift
//  Supermarket
//
//  Created by Valery Rublevskaya on 15/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import Foundation

extension NSObject {    
    class var stringClass: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
    var stringClass: String {
        return NSStringFromClass(type(of: self)).components(separatedBy: ".").last!
    }
}
