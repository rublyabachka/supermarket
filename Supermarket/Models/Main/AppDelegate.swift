//
//  AppDelegate.swift
//  Supermarket
//
//  Created by Valery Rublevskaya on 15/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    lazy private var coreDataStack = CoreDataStack(with: "supermarket")
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        configureApp()
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        coreDataStack.save()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        coreDataStack.save()
    }
    
    fileprivate func configureApp() {
        configureAppearance()
        configureOtherThings()
    }
    
    fileprivate func configureAppearance() {}
    
    fileprivate func configureOtherThings() {
        let documentsPath: String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        print(documentsPath)
        
        guard let tabBarController = self.window?.rootViewController as? SupermarketTabController else { return }
        guard let navController = tabBarController.viewControllers?.first as? UINavigationController else { return }
        guard let purchasesCollectionViewController = navController.topViewController as? PurchasesCollectionViewController else { return }
        purchasesCollectionViewController.coreDataStack = coreDataStack
        
        guard let salesNavController = tabBarController.viewControllers?.last as? UINavigationController else { return }
        guard let salesTableViewController = salesNavController.topViewController as? SalesTableViewController else { return }
        salesTableViewController.coreDataStack = coreDataStack
        
        coreDataStack.populateDataIfNeeded()
        
    }

}

