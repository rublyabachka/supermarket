//
//  Category+CoreDataProperties.swift
//  Supermarket
//
//  Created by Valery Rublevskaya on 20/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import Foundation
import CoreData


extension Category {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Category> {
        return NSFetchRequest<Category>(entityName: "Category")
    }

    @NSManaged public var id: Int32
    @NSManaged public var name: String
    @NSManaged public var goods: NSSet

}

// MARK: Generated accessors for goods
extension Category {

    @objc(addGoodsObject:)
    @NSManaged public func addToGoods(_ value: Good)

    @objc(removeGoodsObject:)
    @NSManaged public func removeFromGoods(_ value: Good)

    @objc(addGoods:)
    @NSManaged public func addToGoods(_ values: NSSet)

    @objc(removeGoods:)
    @NSManaged public func removeFromGoods(_ values: NSSet)

}
