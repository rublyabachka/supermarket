//
//  Category+CoreDataClass.swift
//  Supermarket
//
//  Created by Valery Rublevskaya on 17/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import Foundation
import CoreData

@objc(Category)
public class Category: NSManagedObject {
    static func category(from dictionary: [String: Any], withContext context: NSManagedObjectContext) -> Category? {
        guard let id = dictionary["id"] as? Int32,
            let name = dictionary["name"] as? String else { return nil }
        
        var category: Category?
        context.performAndWait {
            let fetchRequest: NSFetchRequest<Category> = Category.fetchRequest()
            let predicate = NSPredicate(format: "%d == %K", id, #keyPath(Category.id))
            fetchRequest.predicate = predicate
            do {
                let categories = try context.fetch(fetchRequest) 
                if categories.isEmpty {
                    category = Category(context: context)
                    category?.id = id
                    category?.name = name
                    category?.goods = NSSet()
                } else {
                    category = categories.first
                }
            } catch let error as NSError {
                print("Cannot fetch category. Unresolved error  \(error), \(error.userInfo)")
            }
        }
        
        return category
    }
}

extension NSManagedObject {
    static func incrementId<T: NSManagedObject>(type: T.Type, context: NSManagedObjectContext) -> Int32 {
        
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        fetchRequest.entity = NSEntityDescription.entity(forEntityName: type.stringClass, in: context)
        fetchRequest.resultType = .dictionaryResultType
        
        let keypathExpression = NSExpression(forKeyPath: "id")
        let maxExpression = NSExpression(forFunction: "max:", arguments: [keypathExpression])
        
        let key = "maxId"
        
        let expressionDescription = NSExpressionDescription()
        expressionDescription.name = key
        expressionDescription.expression = maxExpression
        expressionDescription.expressionResultType = .integer32AttributeType
        
        fetchRequest.propertiesToFetch = [expressionDescription]
        
        do {
            if let result = try context.fetch(fetchRequest) as? [[String: Int32]], let dict = result.first {
                return dict[key]! + 1
            } else {
                return 0
            }
        } catch {
            assertionFailure("Failed to fetch max id with error = \(error)")
            abort()
        }
    }
}
