//
//  Good+CoreDataClass.swift
//  Supermarket
//
//  Created by Valery Rublevskaya on 17/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import Foundation
import CoreData

@objc(Good)
public class Good: NSManagedObject {
    static func good(from dictionary: [String: Any], withContext context: NSManagedObjectContext) -> Good? {
        guard let id = dictionary["id"] as? Int32,
            let name = dictionary["name"] as? String,
            let price = dictionary["price"] as? Double,
            let count = dictionary["count"] as? Int32,
            let categoryDict = dictionary["category"] as? [String: Any],
            let category = Category.category(from: categoryDict, withContext: context) else { return nil }
        
        var good: Good?
        context.performAndWait {
            good = Good(context: context)
            good!.id = id
            good!.name = name
            good!.price = price
            good!.count = count
            good!.category = category
            category.addToGoods(good!)
        }
        
        return good
    }
}
