//
//  Good+CoreDataProperties.swift
//  Supermarket
//
//  Created by Valery Rublevskaya on 17/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import Foundation
import CoreData

extension Good {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Good> {
        return NSFetchRequest<Good>(entityName: "Good")
    }

    @NSManaged public var id: Int32
    @NSManaged public var name: String
    @NSManaged public var price: Double
    @NSManaged public var count: Int32
    @NSManaged public var category: Category

}
