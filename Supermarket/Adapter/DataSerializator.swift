//
//  DataSerializator.swift
//  Supermarket
//
//  Created by Valery Rublevskaya on 20/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import Foundation

protocol DataSerializator: class {
    func serialize(resource resourceString: String) -> [[String: Any]]?
}
