//
//  JSONSerializator.swift
//  Supermarket
//
//  Created by Valery Rublevskaya on 20/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import Foundation

class JSONSerializator: DataSerializator {
    func serialize(resource resourceString: String) -> [[String: Any]]? {
        guard let url = Bundle.main.url(forResource: resourceString, withExtension: "json") else { return nil }
        var json: Any?
        do {
            let data = try Data(contentsOf: url)
            json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
        } catch let error as NSError {
            print("Cannot populateData. Unresolved error  \(error), \(error.userInfo)")
        }
        guard let array = json as? [[String: Any]] else { return nil }
        return array
    }
}
