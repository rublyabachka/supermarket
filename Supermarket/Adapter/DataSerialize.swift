//
//  DataSerialize.swift
//  Supermarket
//
//  Created by Valery Rublevskaya on 20/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import Foundation

class DataSerialize {
    static func serializeFile(resource resourceString: String, serializator: DataSerializator) -> [[String: Any]]? {
        return serializator.serialize(resource: resourceString)
    }
}
