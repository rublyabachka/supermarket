//
//  XMLSerializator.swift
//  Supermarket
//
//  Created by Valery Rublevskaya on 20/07/2017.
//  Copyright © 2017 Valery Rublevskaya. All rights reserved.
//

import Foundation

class XMLSerializator: DataSerializator {
    func serialize(resource resourceString: String) -> [[String: Any]]? {
        guard let _ = Bundle.main.url(forResource: resourceString, withExtension: "xml") else { return nil }
        // В случае необходимости реализовать парсинг xml-файла
        return []
    }
}
